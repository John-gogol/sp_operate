import request from '@/utils/request'

// 查询品牌列表
export function listPhBranda(query) {
  return request({
    url: '/background/phBranda/list',
    method: 'get',
    params: query
  })
}

// 查询品牌详细
export function getPhBranda(id) {
  return request({
    url: '/background/phBranda/' + id,
    method: 'get'
  })
}

// 新增品牌
export function addPhBranda(data) {
  return request({
    url: '/background/phBranda',
    method: 'post',
    data: data
  })
}

// 修改品牌
export function updatePhBranda(data) {
  return request({
    url: '/background/phBranda',
    method: 'put',
    data: data
  })
}

// 删除品牌
export function delPhBranda(id) {
  return request({
    url: '/background/phBranda/' + id,
    method: 'delete'
  })
}
