import request from '@/utils/request'

// 查询商品列表
export function listPhProduct(query) {
  return request({
    url: '/background/phProduct/list',
    method: 'get',
    params: query
  })
}

// 查询商品详细
export function getPhProduct(id) {
  return request({
    url: '/background/phProduct/' + id,
    method: 'get'
  })
}

// 新增商品
export function addPhProduct(data) {
  return request({
    url: '/background/phProduct',
    method: 'post',
    data: data
  })
}

// 修改商品
export function updatePhProduct(data) {
  return request({
    url: '/background/phProduct',
    method: 'put',
    data: data
  })
}

// 删除商品
export function delPhProduct(id) {
  return request({
    url: '/background/phProduct/' + id,
    method: 'delete'
  })
}

// 批量审核
export function batchAuditing(ids,type) {
  return request({
    url: '/background/auditing/list/'+ ids +'&'+ type,
    method: 'post',
  })
}

// 商品图片
export function getPhProductImg(id) {
  return request({
    url: '/background/phProduct/img/'+ id,
    method: 'get',
  })
}
