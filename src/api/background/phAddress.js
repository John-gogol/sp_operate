import request from '@/utils/request'

// 查询用户地址设置列表
export function listPhAddress(query) {
  return request({
    url: '/background/phAddress/list',
    method: 'get',
    params: query
  })
}

// 查询用户地址设置详细
export function getPhAddress(id) {
  return request({
    url: '/background/phAddress/' + id,
    method: 'get'
  })
}

// 新增用户地址设置
export function addPhAddress(data) {
  return request({
    url: '/background/phAddress',
    method: 'post',
    data: data
  })
}

// 修改用户地址设置
export function updatePhAddress(data) {
  return request({
    url: '/background/phAddress',
    method: 'put',
    data: data
  })
}

// 删除用户地址设置
export function delPhAddress(id) {
  return request({
    url: '/background/phAddress/' + id,
    method: 'delete'
  })
}

// 查询全国地质（国-省-市-区）
export function listPhRegion(query) {
  console.log('chaxun')
  return request({
    url: '/background/phRegion/list',
    method: 'get',
    params: query
  })
}