import request from '@/utils/request'

// 查询标签列表
export function listPhTag(query) {
  return request({
    url: '/background/phTag/list',
    method: 'get',
    params: query
  })
}

// 查询标签详细
export function getPhTag(id) {
  return request({
    url: '/background/phTag/' + id,
    method: 'get'
  })
}

// 新增标签
export function addPhTag(data) {
  return request({
    url: '/background/phTag',
    method: 'post',
    data: data
  })
}

// 修改标签
export function updatePhTag(data) {
  return request({
    url: '/background/phTag',
    method: 'put',
    data: data
  })
}

// 删除标签
export function delPhTag(id) {
  return request({
    url: '/background/phTag/' + id,
    method: 'delete'
  })
}
