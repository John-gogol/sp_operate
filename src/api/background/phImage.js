import request from '@/utils/request'

// 查询商品图片列表
export function listPhImage(query) {
  return request({
    url: '/background/phImage/list',
    method: 'get',
    params: query
  })
}
/** 查询商品图片详情 */ 
export function phImageImg(query) {
  return request({
    url: '/background/phImage/list/img',
    method: 'get',
    params: query
  })
}

// 查询商品图片详细
export function getPhImage(id) {
  return request({
    url: '/background/phImage/' + id,
    method: 'get'
  })
}

// 新增商品图片
export function addPhImage(data) {
  return request({
    url: '/background/phImage',
    method: 'post',
    data: data
  })
}

// 修改商品图片
export function updatePhImage(data) {
  return request({
    url: '/background/phImage',
    method: 'put',
    data: data
  })
}

// 删除商品图片
export function delPhImage(id) {
  return request({
    url: '/background/phImage/' + id,
    method: 'delete'
  })
}
