import request from '@/utils/request'

// 查询物流公司列表
export function listPhPartner(query) {
  return request({
    url: '/background/phPartner/list',
    method: 'get',
    params: query
  })
}

// 查询物流公司详细
export function getPhPartner(id) {
  return request({
    url: '/background/phPartner/' + id,
    method: 'get'
  })
}

// 新增物流公司
export function addPhPartner(data) {
  return request({
    url: '/background/phPartner',
    method: 'post',
    data: data
  })
}

// 修改物流公司
export function updatePhPartner(data) {
  return request({
    url: '/background/phPartner',
    method: 'put',
    data: data
  })
}

// 删除物流公司
export function delPhPartner(id) {
  return request({
    url: '/background/phPartner/' + id,
    method: 'delete'
  })
}
