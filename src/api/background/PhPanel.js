import request from '@/utils/request'

// 查询参数模板列表
export function listPhPanel(query) {
  return request({
    url: '/background/PhPanel/list',
    method: 'get',
    params: query
  })
}

// 查询参数模板详细
export function getPhPanel(id) {
  return request({
    url: '/background/PhPanel/' + id,
    method: 'get'
  })
}

// 新增参数模板
export function addPhPanel(data) {
  return request({
    url: '/background/PhPanel',
    method: 'post',
    data: data
  })
}

// 修改参数模板
export function updatePhPanel(data) {
  return request({
    url: '/background/PhPanel',
    method: 'put',
    data: data
  })
}

// 删除参数模板
export function delPhPanel(id) {
  return request({
    url: '/background/PhPanel/' + id,
    method: 'delete'
  })
}
