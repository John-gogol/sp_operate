import request from '@/utils/request'

// 查询协议列表
export function listPhCurrencyTb(query) {
  return request({
    url: '/background/phCurrencyTb/list',
    method: 'get',
    params: query
  })
}

// 查询协议详细
export function getPhCurrencyTb(id) {
  return request({
    url: '/background/phCurrencyTb/' + id,
    method: 'get'
  })
}

// 新增协议
export function addPhCurrencyTb(data) {
  return request({
    url: '/background/phCurrencyTb',
    method: 'post',
    data: data
  })
}

// 修改协议
export function updatePhCurrencyTb(data) {
  return request({
    url: '/background/phCurrencyTb',
    method: 'put',
    data: data
  })
}

// 删除协议
export function delPhCurrencyTb(id) {
  return request({
    url: '/background/phCurrencyTb/' + id,
    method: 'delete'
  })
}
