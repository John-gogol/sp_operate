import request from '@/utils/request'

// 查询协议确认记录列表
export function listPhSign(query) {
  return request({
    url: '/background/phSign/list',
    method: 'get',
    params: query
  })
}

// 查询协议确认记录详细
export function getPhSign(id) {
  return request({
    url: '/background/phSign/' + id,
    method: 'get'
  })
}

// 新增协议确认记录
export function addPhSign(data) {
  return request({
    url: '/background/phSign',
    method: 'post',
    data: data
  })
}

// 修改协议确认记录
export function updatePhSign(data) {
  return request({
    url: '/background/phSign',
    method: 'put',
    data: data
  })
}

// 删除协议确认记录
export function delPhSign(id) {
  return request({
    url: '/background/phSign/' + id,
    method: 'delete'
  })
}
