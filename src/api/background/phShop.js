import request from '@/utils/request'

// 查询店铺列表
export function listPhShop(query) {
  return request({
    url: '/background/phShop/list',
    method: 'get',
    params: query
  })
}

// 查询店铺会员列表
export function listPhShopMember(query) {
  return request({
    url: '/background/phShop/list/member',
    method: 'get',
    params: query
  })
}

// 查询店铺详细
export function getPhShop(id) {
  const res = request({
    url: '/background/phShop/' + id ,
    method: 'get'
  })
  return res
}

// 新增店铺
export function addPhShop(data) {
  return request({
    url: '/background/phShop',
    method: 'post',
    data: data
  })
}

// 修改店铺
export function updatePhShop(data) {
  return request({
    url: '/background/phShop',
    method: 'put',
    data: data
  })
}

// 删除店铺
export function delPhShop(id) {
  return request({
    url: '/background/phShop/' + id,
    method: 'delete'
  })
}

// 审核
// export function getShopAuditing(data) {
//   const res = request({
//     url: '/background/phShop/auditing/',
//     method: 'post',
//     data: data
//   })
//   return res
// }

// 审核
export function getAuditing(data) {
  return request({
    url: '/background/auditing',
    method: 'put',
    data: data
  })
}