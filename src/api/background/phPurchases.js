import request from '@/utils/request'

// 查询采购询价列表
export function listPhPurchases(query) {
  return request({
    url: '/background/phPurchases/list',
    method: 'get',
    params: query
  })
}
// 查询采购报价列表
export function listQuoteprice(query) {
  return request({
    url: '/background/quoteprice/list',
    method: 'get',
    params: query
  })
}

// 查询采购询价详细
export function getPhPurchases(id) {
  return request({
    url: '/background/phPurchases/' + id,
    method: 'get'
  })
}

// 新增采购询价
export function addPhPurchases(data) {
  return request({
    url: '/background/phPurchases',
    method: 'post',
    data: data
  })
}

// 修改采购询价
export function updatePhPurchases(data) {
  return request({
    url: '/background/phPurchases',
    method: 'put',
    data: data
  })
}

// 删除采购询价
export function delPhPurchases(id) {
  return request({
    url: '/background/phPurchases/' + id,
    method: 'delete'
  })
}
