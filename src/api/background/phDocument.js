import request from '@/utils/request'

// 查询文档列表
export function listPhDocument(query) {
  return request({
    url: '/background/phDocument/list',
    method: 'get',
    params: query
  })
}

// 查询文档详细
export function getPhDocument(id) {
  return request({
    url: '/background/phDocument/' + id,
    method: 'get'
  })
}

// 新增文档
export function addPhDocument(data) {
  return request({
    url: '/background/phDocument',
    method: 'post',
    data: data
  })
}

// 修改文档
export function updatePhDocument(data) {
  return request({
    url: '/background/phDocument',
    method: 'put',
    data: data
  })
}

// 删除文档
export function delPhDocument(id) {
  return request({
    url: '/background/phDocument/' + id,
    method: 'delete'
  })
}
