import request from '@/utils/request'

// 查询物流进度列表
export function listPhProgress(query) {
  return request({
    url: '/background/phProgress/list',
    method: 'get',
    params: query
  })
}

// 查询物流进度详细
export function getPhProgress(id) {
  return request({
    url: '/background/phProgress/' + id,
    method: 'get'
  })
}

// 新增物流进度
export function addPhProgress(data) {
  return request({
    url: '/background/phProgress',
    method: 'post',
    data: data
  })
}

// 修改物流进度
export function updatePhProgress(data) {
  return request({
    url: '/background/phProgress',
    method: 'put',
    data: data
  })
}

// 删除物流进度
export function delPhProgress(id) {
  return request({
    url: '/background/phProgress/' + id,
    method: 'delete'
  })
}
