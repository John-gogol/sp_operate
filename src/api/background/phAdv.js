import request from '@/utils/request'

// 查询轮播广告配置列表
export function listPhAdv(query) {
  return request({
    url: '/background/phAdv/list',
    method: 'get',
    params: query
  })
}

// 查询轮播广告配置详细
export function getPhAdv(id) {
  return request({
    url: '/background/phAdv/' + id,
    method: 'get'
  })
}

// 新增轮播广告配置
export function addPhAdv(data) {
  return request({
    url: '/background/phAdv',
    method: 'post',
    data: data
  })
}

// 修改轮播广告配置
export function updatePhAdv(data) {
  return request({
    url: '/background/phAdv',
    method: 'put',
    data: data
  })
}

// 删除轮播广告配置
export function delPhAdv(id) {
  return request({
    url: '/background/phAdv/' + id,
    method: 'delete'
  })
}
