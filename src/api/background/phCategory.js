import request from '@/utils/request'

// 查询商品类目列表
export function listPhCategory(query) {
  return request({
    url: '/background/phCategory/list',
    method: 'get',
    params: query
  })
}

// 查询商品类目详细
export function getPhCategory(id) {
  return request({
    url: '/background/phCategory/' + id,
    method: 'get'
  })
}

// 新增商品类目
export function addPhCategory(data) {
  return request({
    url: '/background/phCategory',
    method: 'post',
    data: data
  })
}

// 修改商品类目
export function updatePhCategory(data) {
  return request({
    url: '/background/phCategory',
    method: 'put',
    data: data
  })
}

// 删除商品类目
export function delPhCategory(id) {
  return request({
    url: '/background/phCategory/' + id,
    method: 'delete'
  })
}
