import request from '@/utils/request'

// 查询模板列表
export function listPhTemplate(query) {
  return request({
    url: '/background/phTemplate/list',
    method: 'get',
    params: query
  })
}

// 查询模板详细
export function getPhTemplate(id) {
  return request({
    url: '/background/phTemplate/' + id,
    method: 'get'
  })
}
// 查询模板详细
export function getPhTemplateCategoryId(id) {
  return request({
    url: '/background/phTemplate/Info/' + id,
    method: 'get'
  })
}

// 新增模板
export function addPhTemplate(data) {
  return request({
    url: '/background/phTemplate',
    method: 'post',
    data: data
  })
}

// 修改模板
export function updatePhTemplate(data) {
  return request({
    url: '/background/phTemplate',
    method: 'put',
    data: data
  })
}

// 删除模板
export function delPhTemplate(id) {
  return request({
    url: '/background/phTemplate/' + id,
    method: 'delete'
  })
}
