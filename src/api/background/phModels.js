import request from '@/utils/request'

// 查询型号库列表
export function listPhModels(query) {
  return request({
    url: '/background/phModels/list',
    method: 'get',
    params: query
  })
}

// 查询型号库详细
export function getPhModels(module) {
  return request({
    url: '/background/phModels/' + module,
    method: 'get'
  })
}
// 查询旧型号库的参数
export function getPhModeule(module) {
  return request({
    url: '/background/modeule/' + module,
    method: 'get'
  })
}

// 新增型号库
export function addPhModels(data) {
  return request({
    url: '/background/phModels',
    method: 'post',
    data: data
  })
}

// 修改型号库
export function updatePhModels(data) {
  return request({
    url: '/background/phModels',
    method: 'put',
    data: data
  })
}

// 删除型号库
export function delPhModels(id) {
  return request({
    url: '/background/phModels/' + id,
    method: 'delete'
  })
}

