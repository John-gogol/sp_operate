import request from '@/utils/request'

// 查询审核文档列表
export function listPhDocumentinfo(query) {
  return request({
    url: '/background/phDocumentinfo/list',
    method: 'get',
    params: query
  })
}

// 查询审核文档详细
export function getPhDocumentinfo(id) {
  return request({
    url: '/background/phDocumentinfo/' + id,
    method: 'get'
  })
}

// 新增审核文档
export function addPhDocumentinfo(data) {
  return request({
    url: '/background/phDocumentinfo',
    method: 'post',
    data: data
  })
}
// 新增审核文档
export function addEmbody(data) {
  return request({
    url: '/background/phDocumentinfo/embody',
    method: 'post',
    data: data
  })
}

// 修改审核文档
export function updatePhDocumentinfo(data) {
  return request({
    url: '/background/phDocumentinfo',
    method: 'put',
    data: data
  })
}

// 删除审核文档
export function delPhDocumentinfo(id) {
  return request({
    url: '/background/phDocumentinfo/' + id,
    method: 'delete'
  })
}
