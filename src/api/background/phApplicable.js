import request from '@/utils/request'

// 查询场景列表
export function listPhApplicable(query) {
  return request({
    url: '/background/phApplicable/list',
    method: 'get',
    params: query
  })
}

// 查询场景详细
export function getPhApplicable(id) {
  return request({
    url: '/background/phApplicable/' + id,
    method: 'get'
  })
}

// 新增场景
export function addPhApplicable(data) {
  return request({
    url: '/background/phApplicable',
    method: 'post',
    data: data
  })
}

// 修改场景
export function updatePhApplicable(data) {
  return request({
    url: '/background/phApplicable',
    method: 'put',
    data: data
  })
}

// 删除场景
export function delPhApplicable(id) {
  return request({
    url: '/background/phApplicable/' + id,
    method: 'delete'
  })
}
