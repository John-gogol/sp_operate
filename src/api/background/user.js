import request from '@/utils/request'

// 查询中心用户列表
export function listUser(query) {
  console.log(query)
  return request({
    url: '/background/centerUser/list',
    method: 'get',
    params: query
  })
}

// 查询中心用户详细
export function getUser(id) {
  return request({
    url: '/background/centerUser/' + id,
    method: 'get'
  })
}

// 新增中心用户
export function addUser(data) {
  return request({
    url: '/background/centerUser',
    method: 'post',
    data: data
  })
}

// 修改中心用户
export function updateUser(data) {
  return request({
    url: '/background/centerUser',
    method: 'put',
    data: data
  })
}

// 删除中心用户
export function delUser(id) {
  return request({
    url: '/background/centerUser/' + id,
    method: 'delete'
  })
}
