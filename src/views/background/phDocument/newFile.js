import { listPhDocument, getPhDocument, delPhDocument, addPhDocument, updatePhDocument } from "@/api/background/phDocument";

export default (await import('vue')).defineComponent({
name: "PhDocument",
data() {
return {
// 遮罩层
loading: true,
// 选中数组
ids: [],
// 非单个禁用
single: true,
// 非多个禁用
multiple: true,
// 显示搜索条件
showSearch: true,
// 总条数
total: 0,
// 文档表格数据
phDocumentList: [],
// 弹出层标题
title: "",
// 是否编辑
editStaues: false,
// 是否显示弹出层
open: false,
// 查询参数
queryParams: {
pageNum: 1,
pageSize: 10,
documentName: null,
type: null,
version: null,
language: null,
formerly: null,
popedom: null,
display: null,
dl: null,
updatePeople: null,
},
// 表单参数
form: {},
// 表单校验
rules: {
createTime: [
{ required: true, message: "创建时间不能为空", trigger: "blur" }
],
updateTime: [
{ required: true, message: "更新时间不能为空", trigger: "blur" }
]
},
// 详情页
documentDetailsPop: null,

previewPop: false,
previewUrl: '', //预览URL
};
},
created() {
this.getList();
},
methods: {
/** 查询文档列表 */
getList() {
this.loading = true;
listPhDocument(this.queryParams).then(response => {
// this.phDocumentList = this.redoList(response.rows);  //重构
this.phDocumentList = this.setDisplay(response.rows); //转换状态显示类型
this.total = response.total;
this.loading = false;
console.log(this.phDocumentList);
});
},
// 取消按钮
cancel() {
this.open = false;
this.reset();
},
// 表单重置
reset() {
this.form = {
id: null,
documentName: null,
type: null,
version: null,
language: null,
formerly: null,
popedom: null,
display: null,
dl: null,
updatePeople: null,
createTime: null,
updateTime: null
};
this.resetForm("form");
},
/** 搜索按钮操作 */
handleQuery() {
this.queryParams.pageNum = 1;
this.getList();
},
/** 重置按钮操作 */
resetQuery() {
this.resetForm("queryForm");
this.handleQuery();
},
// 多选框选中数据
handleSelectionChange(selection) {
this.ids = selection.map(item => item.id);
this.single = selection.length !== 1;
this.multiple = !selection.length;
},
/** 新增按钮操作 */
handleAdd() {
this.reset();
this.form.type = 1; //默认规格书
this.form.language = 2; //默认简体中文
this.open = true;
this.editStaues = true;
this.title = "添加文档";
},
/** 修改按钮操作 */
handleUpdate(row) {
this.reset();
const id = row.id || this.ids;
getPhDocument(id).then(response => {
this.form = response.data;
this.open = true;
this.editStaues = true;
this.title = "修改文档";
let lastIndex = this.form.documentUrl.lastIndexOf('/');
this.form.documentUrlName = this.form.documentUrl.substring(lastIndex + 1);
});
},
/** 详情按钮操作 */
handDetails(row) {
this.reset();
const id = row.id || this.ids;
getPhDocument(id).then((response) => {
this.form = response.data;
this.open = true;
this.editStaues = false;
this.title = "详情文档";
let lastIndex = this.form.documentUrl.lastIndexOf('/');
this.form.documentUrlName = this.form.documentUrl.substring(lastIndex + 1);
});
},
/** 提交按钮 */
submitForm() {
this.$refs["form"].validate(valid => {
if (valid) {
let newDocumentName = this.form.modelName + '(' + this.form.version + ').pdf';
this.form.documentName = newDocumentName;
if (this.form.id != null) {
updatePhDocument(this.form).then(response => {
this.$modal.msgSuccess("修改成功");
this.open = false;
this.getList();
});
} else {
addPhDocument(this.form).then(response => {
this.$modal.msgSuccess("新增成功");
this.open = false;
this.getList();
});
}
}
});
},

/** 禁用状态转换 */
setDisplay(data) {
let list = data;
for (let item of list) {
item.display = item.display == 1 ? true : false; //转换状态显示类型
}
return list;
},
// 列表开关回调-是否显示
switchDisplay(id, sta) {
// 查询修改接口
getPhDocument(id).then((response) => {
this.form = response.data;
this.form.display = sta ? 1 : 0; //判断开关状态

// 提交修改接口
updatePhDocument(this.form).then(response => {
this.$modal.msgSuccess("修改成功");
this.open = false;
this.getList();
});
});
},
// 用户详情返回列表按钮
detailsBack() {
this.documentDetailsPop = false;
},
/** 删除按钮操作 */
handleDelete(row) {
const ids = row.id || this.ids;
this.$modal.confirm('是否确认删除文档编号为"' + ids + '"的数据项？').then(function () {
return delPhDocument(ids);
}).then(() => {
this.getList();
this.$modal.msgSuccess("删除成功");
}).catch(() => { });
},
/** 导出按钮操作 */
handleExport() {
this.download('background/phDocument/export', {
...this.queryParams
}, `phDocument_${new Date().getTime()}.xlsx`);
},
// 修改状态
// setEditStaues() {
//   this.editStaues = true
// },
// 下载文档
downloadFile(url) {
console.log(url);
let lastIndex = url.lastIndexOf('/');
let urlName = url.substring(lastIndex + 1);
// 创建一个Blob对象  
const blob = new Blob([url], { type: 'application/pdf' });
// 创建一个可下载的URL  
const downloadUrl = window.URL.createObjectURL(blob);
// 创建一个a标签，并设置href为URL，模拟点击下载  
console.log(downloadUrl);
const link = document.createElement('a');
link.href = downloadUrl;
// link.href = url;  
link.download = urlName; // 设置下载文件的文件名 
console.log(urlName);
link.click();
// 释放URL对象  
URL.revokeObjectURL(downloadUrl);
},
// 浏览文件
previewFile(url) {
this.previewUrl = url;
this.previewPop = true;
},
}
});
